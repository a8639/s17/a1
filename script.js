let students = [];

function addStudent(name){
    students.push(name);
};
addStudent("John");
console.log(`${students[0]} was added to the student's list`);
addStudent("Jane");
console.log(`${students[1]} was added to the student's list`);
addStudent("Joe");
console.log(`${students[2]} was added to the student's list`);

console.log(students.length);

students.forEach(
    function(element){
        console.log(element)
    }
);

let printStudents = students.sort(
    function(a, b){
        return a - b
    }
);
console.log(printStudents);


function findStudent(studentName){
    if (students.includes(studentName)){
        return `${studentName} is an enrollee`;
    } else {
        return `no student was found with name ${studentName}`;
    }
}
console.log(findStudent("John"));
console.log(findStudent("joe"));


